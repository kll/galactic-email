# Galactic Email

## Assumptions

- security is out of  scope as it was not mentioned in the assignment

- UI does not need to be able to define the fields that are available in the templates

- can use an existing mediator library to implement the message bus

## Needs

- need to provide API endpoints for the UI to consume

  - READ available template fields

  - CRUD for templates

  - CRUD for scheduled emails

- templates


  - standard email fields (to, from, body, subject)

    - each field must be rendered to replace templated fields

  - event to respond to

  - optional scheduled time

- emails

  - must be sent async

  - based on a template

- events

  - message bus

  - handlers respond to events to send the emails

  - background worker to fire events for scheduled emails

  # Known Incomplete Stuff

  - The actual email sending bits were not fully fleshed out

  - Some of of the interfaces do not have concrete implementations

  - Connections in diagram are extremely messy
  